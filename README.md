Object Oriented Software Programming Project # 2

Written in Java

The project is an arcade game where a player can control a frog. The player must avoid collisions with hazards (e.g. buses) to avoid losing a life and complete the game. The frog player's goal is to cross the road successfully.