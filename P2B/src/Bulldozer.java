import org.newdawn.slick.Input;

/**
 * The bulldozer is able to push the player upon
 * collision.
 */

public class Bulldozer extends Vehicle {
	/** Address of the bulldozer image */
	public final static String ASSET_PATH = "assets/bulldozer.png";
	
	private final static float SPEED = 0.2f;
	
	private boolean moveRight;
	
	/** Initialises the bulldozer */
	public Bulldozer(String path, float x, float y, boolean moveRight, String type) {
			super(path, x, y, moveRight, type);
			
			this.moveRight = moveRight;
			
	}
	
	@Override
	public void update(Input input, int delta) {
		move(SPEED * delta * (moveRight ? 1 : -1), 0);
		
		// check if the vehicle has moved off the screen
		// this will get repeptitive , need better abstarction */
		if (getX() > App.SCREEN_WIDTH + World.TILE_SIZE / 2 || getX() < -World.TILE_SIZE / 2
		 || getY() > App.SCREEN_HEIGHT + World.TILE_SIZE / 2 || getY() < -World.TILE_SIZE / 2) {
			setX(getInitialX());
		}
	}
	
	@Override
	public void onCollision(Sprite other) {
		// push player
		if (getY() == other.getY() && getX() + World.TILE_SIZE/2 < other.getX()) {
			other.setX(getX() + World.TILE_SIZE);
			other.setY(getY());
			((Player)other).setCanMove(true);
		}
	}
}

