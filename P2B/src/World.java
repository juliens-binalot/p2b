
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/** Class that handles the updates, rendering
 * and collision of sprites in the current level.
 */
public class World {
	
	/** Tile size of the game */
	public static final int TILE_SIZE = 48;
	
	private Level level;
	private int levelNum = 0;
	
	/** Initialises the current level
	 * @param path The address of the .lvl file.
	 * @param levelNum The number of the current level.
	 */
	public World(String path, int levelNum) {
		
		// load level
		level = Level.loadLevel(path);
		this.levelNum = levelNum;

	}
	
	/** Update the sprites, sprite collisions and level state for a frame.
     * @param input The arrow keys
     * @param delta Time passed since last frame (milliseconds).
     */
	public void update(Input input, int delta) throws SlickException {
		for (Sprite sprite : level.getSprites()) {
			sprite.update(input, delta);
		}
		
		// loop over all pairs of sprites and test for intersection
		for (Sprite sprite1: level.getSprites()) {
			for (Sprite sprite2: level.getSprites()) {
				if (sprite1 != sprite2
						&& sprite1.collides(sprite2)) {
					sprite1.onCollision(sprite2);
				}
			}
		}
		
		
		// is level complete? if so call setup level
		if (level.isLevelComplete()) {
			levelNum += 1;
			if (levelNum > 1) {
				System.exit(0);
			}
			App.setWorld(loadWorld(levelNum));
		}
		
		level.update(input, delta);
	}
	
	/** Render the entire screen, so it reflects the current game state.
     * @param g The Slick graphics object, used for drawing.
     */
	public void render(Graphics g){
		
		// render all sprites
		for (Sprite sprite : level.getSprites()) {
			if (sprite instanceof Turtle && !((Turtle) sprite).getOnSurface()) {
				// skip
			} else {
				sprite.render();
			}
		}
		
		for (Sprite sprite : level.getSpriteLives()) {
			sprite.render();
		}
	}
	
	/** Loads the world for current level
	 *  Called by by App.init() and World.update()
	 * @param levelNum The current level number.
	 * @return Returns the World for each level.
	 */
	public static World loadWorld(int levelNum) {
		// initialise world
		switch (levelNum) {
		case 0:
			return new World("assets/res/0.lvl", levelNum);
		case 1:
			return new World("assets/res/1.lvl", levelNum);
		}
		return null;
	}
}
