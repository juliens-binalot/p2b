
/**
 * A class that makes the environment of the game
 *
 */
public class Tile extends Sprite {
	private static final String GRASS_PATH = "assets/grass.png";
	private static final String WATER_PATH = "assets/water.png";
	private static final String TREE_PATH = "assets/tree.png";
	
	/**
	 * Creates a grass Tile
	 * @param x
	 * @param y
	 * @return Returns a Tile object
	 */
	public static Tile createGrassTile(float x, float y) {
		return new Tile(GRASS_PATH, x, y);
	}
	
	/**
	 * Creates a water tile
	 * @param x
	 * @param y
	 * @return Returns a tile object
	 */
	public static Tile createWaterTile(float x, float y) {
		//System.out.println(WATER_PATH);
		return new Tile(WATER_PATH, x, y, new String[] { Sprite.HAZARD });
	}
	
	/**
	 * Creates a tree tile
	 * @param x
	 * @param y
	 * @return Returns a tile object
	 */
	public static Tile createTreeTile(float x, float y) {
		//System.out.println(WATER_PATH);
		return new Tile(TREE_PATH, x, y, new String[] { Sprite.SOLID });
	}
	
	/**
	 * Initialises the grass / water tile.
	 * @param imageSrc The address of the Tile image
	 * @param x
	 * @param y
	 */
	private Tile(String imageSrc, float x, float y) {		
		super(imageSrc, x, y);
	}
	
	/**
	 * Initialises the tree (solid) tile.
	 * @param imageSrc
	 * @param x
	 * @param y
	 * @param tags Tile of type Solid
	 */
	private Tile(String imageSrc, float x, float y, String[] tags) {		
		super(imageSrc, x, y, tags);
	}
	
	
}
	