import org.newdawn.slick.Input;

public class Bike extends Vehicle {
	
	/** Address of the bike image */
	public static String ASSET_PATH = "assets/bike.png";
	
	private static float SPEED = 0.2f;
	
	private boolean moveRight;
	
	/** 
	 * Initialises the bike sprite.
	 * @param path Address of the bike image.
	 * @param x 
	 * @param y
	 * @param moveRight
	 * @param type Sprite is of type hazard.
	 */
	public Bike(String path, float x, float y, boolean moveRight, String type) {
		super(path, x, y, moveRight, type);
		
			this.moveRight = moveRight;
	}
	
	/**
	 * Updates the position of the bike on screen.
	 */
	@Override
	public void update(Input input, int delta) {
		move(SPEED * delta * (moveRight ? 1 : -1), 0);
		
		/* reverse direction */
		if (getX() <= 24) {
			this.moveRight = true;
			setX(24);
		}
		else if (getX() >= 1000){
			this.moveRight = false;
			setX(1000);
		}
	}
}
