import org.newdawn.slick.Input;

/**
 * A rideable class that vanishes after SURFACE_TIME
 *
 */
public class Turtle extends RideableObject {
	
	/** Address of the image of turtles. */
	public final static String ASSET_PATH = "assets/turtles.png";
	
	private final static float SPEED = 0.085f;
	
	// the time specified for a turtle to be visible, and under water
	private final static float SURFACE_TIME = 7000;
	private final static float UNDERWATER_TIME = 2000;
	
	private boolean moveRight;
	
	private float speed;
	
	// under water flag
	private boolean onSurface = true;
	
	private float time = 0;
	
	/** Initialises turtle sprite */
	public Turtle(String path, float x, float y, boolean moveRight, String type) {
			super(path, x, y, moveRight, type);
			this.moveRight = moveRight;
			speed = SPEED;
			
	}
	
	@Override
	public void update(Input input, int delta) {
		move(SPEED * delta * (moveRight ? 1 : -1), 0);
		
		// check if the objects has moved off the screen
		if (getX() > App.SCREEN_WIDTH  + World.TILE_SIZE * 2 || getX() < -World.TILE_SIZE * 2
		 || getY() > App.SCREEN_HEIGHT + World.TILE_SIZE / 2|| getY() < -World.TILE_SIZE / 2) {
			setX(getInitialX());
		}
		
		// if on surface for > SURFACE TIME, take a dive
		time += delta;
		if (onSurface) {
			if (time >= SURFACE_TIME) {
				time = 0;
				onSurface = false;
			}
			
		} else {
			if (time >= UNDERWATER_TIME) {
				onSurface = true;
				time = 0;
			}
		}
	}
	
	/** Access the turtles' speed 
	 * 
	 * @return Returns speed of the turtle
	 */
	public float getSpeed() {
		return speed;
	}
	
	/**
	 * Accesses the flag onSurface 
	 * @return true if the turtles are above water else, false.
	 */
	public boolean getOnSurface() {
		return onSurface;
	} 
	

}
