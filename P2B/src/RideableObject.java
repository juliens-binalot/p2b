import org.newdawn.slick.Input;

/**
 * A class that player can hop on. 
 * Contains log and long log sprites.
 *
 */

public class RideableObject extends Sprite {
	
	/** Addresses of the sprite images */
	public static final String LOG_PATH  = "assets/log.png";
	public static final String LONGLOG_PATH = "assets/longlog.png";
	public static final String TURTLES_PATH = "assets/turtles.png";
	
	private static final float LOG_SPEED = 0.1f;
	private static final float LONGLOG_SPEED = 0.07f;
	
	private float speed;
	
	private static RideableObject log;
	private static RideableObject longLog;
	
	private boolean moveRight;
	
	private boolean hasExtraLife = false;
	private ExtraLife extraLife;
	
	private Player player;
	
	/**
	 * Initialises the rideable object
	 * @param type Is of type RIDEABLE
	 */
	public RideableObject(String imageSrc, float x, float y, boolean moveRight, String type){
		super(imageSrc, x, y, new String[] { type });
		
		this.moveRight = moveRight;
	}
	
	/**
	 * Creates a new log object
	 * @return Returns a new Log object
	 */
	public static RideableObject createLog(String imageSrc, float x, float y, boolean dir, String type) {
		log = new RideableObject(imageSrc, x, y, dir, type);
		log.speed = LOG_SPEED;
		return log;
	}
	
	/**
	 * Creates a long log object
	 * @return Returns a long log object
	 */
	public static RideableObject createLongLog(String imageSrc, float x, float y, boolean dir, String type) {
		longLog = new RideableObject(imageSrc, x, y, dir, type);
		longLog.speed = LONGLOG_SPEED;
		return longLog;
	}
	
	@Override
	public void update(Input input, int delta) {
		move(speed * delta * (moveRight ? 1 : -1), 0);
		
		// check if the objects has moved off the screen
		if (getX() > App.SCREEN_WIDTH  + World.TILE_SIZE * 2 || getX() < -World.TILE_SIZE * 2
		 || getY() > App.SCREEN_HEIGHT + World.TILE_SIZE / 2|| getY() < -World.TILE_SIZE / 2) {
			setX(getInitialX());
			// if on collision with extra life reset extra life's position too
			if (this.hasExtraLife) {
				extraLife.setX(getInitialX());
			}
		}
		
		
	}
	
	public boolean onScreenLog() {
		return !(getX() > App.SCREEN_WIDTH + World.TILE_SIZE *2|| getX() < -World.TILE_SIZE * 2);
	}
	
	/**
	 * Accesses the initial position of the rideable object on screen/
	 * @return Returns the initial x position
	 */
	public final float getInitialX() {
		return moveRight ? -World.TILE_SIZE * 2 
						 : App.SCREEN_WIDTH + World.TILE_SIZE * 2;
	}
	
	public void onCollision(Sprite other) {
		
		if (other.hasTag(Sprite.LIFE)) {
			hasExtraLife = true;
			extraLife = ((ExtraLife)other);
		}
		
		if (!other.hasTag(Sprite.HAZARD) && !other.hasTag(Sprite.LIFE)) {
			player = ((Player)other);
			player.setImmunity(true);
			//System.out.println("player?");
		}
	}
	
	/** 
	 * Accesses the speed of the parameter obj
	 * @param obj The Rideable object
	 * @return Returns the speed of the oj
	 */
	public static float getSpeed(RideableObject obj) {
		return obj.speed;
	}
	
	/** 
	 * Accesses the direction of the sprite
	 * @return Returns true if moving right else false
	 */
	public boolean getDir() {
		return moveRight;
	}
	
}
