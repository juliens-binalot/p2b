import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Class that creates and handles all the sprites 
 * and their collission in the current level.
 *
 */
public class Level {
	
	/* player */
	private ArrayList<Sprite> sprites = new ArrayList<>();
	private Player player;
	private final static String PLAYER_PATH = "assets/frog.png";
	
	/* player lives */
	private ArrayList<Sprite> spriteLives = new ArrayList<>();
	private static final String LIFE_PATH = "assets/lives.png";
	private static final int LIFE_X = 24;
	private static final int LIFE_Y = 744;
	private static final int LIVES_GAP = 32;
	
	/** Number of arguments for Tile objects */
	public static final int TILE_INPUT = 3;
	
	/* holes goal */
	private static final float[] HOLES_X = new float[]{48, 240, 432, 624, 816};
	private static final float HOLES_Y = 48f;
	private int holesFilled = 0;
	public static final int numHoles = 5;
	
	/* time range chosen for extra life*/
	private static final int RAND_TIME_MIN = 25;
	private static final int RAND_TIME_MAX = 35;
	private static final int EXTRALIFE_TIME_DISAPPEAR = 14000;
	private int extraLifeAppearTime;
	
	private ExtraLife extraLife;
	private boolean extraLifeVisible = false;
	
	RideableObject chosenRideable;
	
	private int timeElapsed = 0;
	
	private Random rand = new Random();
	
	/** Initialises the level by reading .lvl file.
	 *  Creates the sprite as specified by the file.
	 * @param path The address of the .lvl file
	 */
	public Level(String path) {
		
		/* read file */
		try (BufferedReader reader = new BufferedReader(new FileReader(path))){
			
			String text;
			while ((text = reader.readLine()) != null) {
				String[] line = text.split(",");
				
				/* create Tiles */
				if (line.length == TILE_INPUT) {
					sprites.add(Sprite.createSprite(line[0], Float.parseFloat(line[1]), 
							Float.parseFloat(line[2])));
				} else {
					/* create Vehicles and Rideable Objects */
					sprites.add(Sprite.createSprite(line[0], Float.parseFloat(line[1]), 
							Float.parseFloat(line[2]), Boolean.parseBoolean(line[3])));
				}
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// create player and add into sprite list
		player = new Player(App.SCREEN_WIDTH / 2, App.SCREEN_HEIGHT - World.TILE_SIZE);
		sprites.add(player);
		
		// create player lives and add into sprite list
		for (int i = LIFE_X; i<player.getLives()*LIVES_GAP; i+=LIVES_GAP) {
			spriteLives.add(new Sprite(LIFE_PATH, i, LIFE_Y));
		}
		
		// choose random time for ExtraLife appearance
		extraLifeAppearTime = (rand.nextInt(RAND_TIME_MAX - RAND_TIME_MIN) + RAND_TIME_MIN)*1000;
		/* for debugging 
		System.out.println(extraLifeAppearTime);*/
	}
	
	/** 
	 * Calls the level constructor when player reaches a new level
	 * @param path The address of the .lvl file
	 * @return Returns a Level
	 */
	public static Level loadLevel(String path) {
		
		return new Level(path);
		
	}
	
	/** Update the time elapsed since an extra life appeared/disappeared,
	 *  existence of extra life, the holes filled, player and its lives
     * @param input The arrow keys.
     * @param delta Time passed since last frame (milliseconds).
     */
	public void update(Input input, int delta) throws SlickException {
		
		updateHolesFilled();
		updatePlayer();
		updatePlayerLives();
		
		// update time elapsed
		timeElapsed += delta;
		if (timeElapsed >= extraLifeAppearTime && !extraLifeVisible) {
			timeElapsed = 0;
			
			// choose log or longlog object in sprites list
			chosenRideable = chooseLog();
			while ((chosenRideable instanceof Turtle )) {
				// skip
				chosenRideable = chooseLog();
			}
			
			extraLife = ((ExtraLife)Sprite.createSprite("extraLife", chosenRideable.getX(), 
					chosenRideable.getY(), chosenRideable.getDir()));
			sprites.add(extraLife);
			
			// set extraLife speed same as chosen rideable object
			extraLife.setSpeed(RideableObject.getSpeed(chosenRideable));
			extraLifeVisible = true;
			
		}
		
		// remove extra life from sprite list after elapsed time >= 14s	
		else if (timeElapsed >= EXTRALIFE_TIME_DISAPPEAR  && extraLifeVisible) {
			sprites.remove(extraLife);
			//System.out.println("disappear");
			
			timeElapsed = 0; 
			extraLifeVisible = false;
		}
		
		if (extraLifeVisible) {
			updateExtraLife();
		}
		
	}
	
	/**
	 * Chooses the log or long log that the extra life will appear on
	 * @return Returns a log or long log
	 */
	private RideableObject chooseLog() {
		
		int index = rand.nextInt(sprites.size());
		
		while (!(sprites.get(index) instanceof RideableObject)) {
			// skip
			index = rand.nextInt(sprites.size());
		}
		
		return ((RideableObject)sprites.get(index));
	}
	
	/**
	 * Updates the holes filled by the player.
	 * Also draws the frog sprite in the holes when they are reached.
	 */
	private void updateHolesFilled() {
		
		for (float hole: HOLES_X) {
			if (player.getX() > hole && player.getX() <= hole + World.TILE_SIZE*2 
					&& player.getY() == HOLES_Y) {
								
				// draw frog in center of hole
				sprites.add(new Sprite(PLAYER_PATH, hole+ World.TILE_SIZE/2 
						+ World.TILE_SIZE, HOLES_Y));
				
				// restart frog position
				player.restartPos();
				holesFilled++;
			}
		}
		
	}
	
	/** 
	 * Checks if a player is on a log or long log, then sets its immunity
	 * to hazards.
	 */
	private void updatePlayer() {
		
		if (player.getRideable() != null) {
			if (!player.collides(player.getRideable())) {
				player.setImmunity(false);
			}
		}
	}
	
	private void updatePlayerLives() {
		
		if (player.getLives() != player.getPrevLives()) {
			int newLives;
			newLives = player.getLives() - player.getPrevLives();
			
			if (newLives < 0) {
				// lost a life
				spriteLives.remove(spriteLives.size() - 1);
			
			} else if (newLives > 0) {
				// gained a life
				spriteLives.add(new Sprite(LIFE_PATH, LIFE_X +
						LIVES_GAP*(player.getLives()-1), LIFE_Y));
			}
		}
		/* for debugging only: 
		  System.out.format("%d %d\n", player.getLives(), player.getPrevLives());
		 */
	}
	
	private void updateExtraLife() {
			
		if (extraLife.collides(player)) {
			sprites.remove(extraLife);
		}
				
		if (!extraLife.collides(chosenRideable)) {
			// if extra life is moving right
			if (extraLife.getDirOnRideable()) {
				extraLife.setDirOnRideable(false);
				extraLife.setX(extraLife.getPrevX()-World.TILE_SIZE);
			
			} else {
				extraLife.setDirOnRideable(true);
				extraLife.setX(extraLife.getPrevX()+World.TILE_SIZE);
			}
		}
		
	}
	
	/**
	 * Called by Level.update(). Checks if level is complete,
	 * based on holes filled by the frog sprite.
	 * @return Returns true/false depending on the holesFilled
	 */
	public boolean isLevelComplete() {
		if (holesFilled == numHoles) {
			return true;
		}
		return false;
	}
	
	/**
	 * Getter for sprite list.
	 * @return Returns an ArrayList of Sprite
	 */
	public ArrayList<Sprite> getSprites(){
		return sprites;
	}
	
	/**
	 * Getter for the lives of player.
	 * @return Returns an ArrayList of Sprite
	 */
	public ArrayList<Sprite> getSpriteLives(){
		return spriteLives;
	}
	
	
}
