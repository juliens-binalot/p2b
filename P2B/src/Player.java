import org.newdawn.slick.Input;

/**Player is  a controllable class */

public class Player extends Sprite {
	private static final String ASSET_PATH = "assets/frog.png";
	
	private static final int INITIAL_LIVES = 3;
	private int lives = INITIAL_LIVES;
	private int prevLives = lives;
	
	/* Sentinel when speed is not applicable*/
	private float speed = -1f;
	
	private boolean moveRight;
	
	/* flag for when colliding with solid objects */
	private boolean canMove = true;
	
	/* flag for when on a rideable object */
	private boolean immune = false;
	
	private RideableObject rideable;
	
	private float prevX;
	private float prevY;
	
	/**
	 * Initialises the player sprite
	 * @param x The x position of the player on screen.
	 * @param y The y position of the player on screen.
	 */
	
	public Player(float x, float y) {
		super(ASSET_PATH, x, y);
	}
	
	/**
	 * Updates the position of the player depending on
	 * on keys pressed.
	 */
	@Override
	public void update(Input input, int delta) {
		
		int dx = 0,
			dy = 0;
		if (input.isKeyPressed(Input.KEY_LEFT)) {
			dx -= World.TILE_SIZE;
		}
		if (input.isKeyPressed(Input.KEY_RIGHT)) {
			dx += World.TILE_SIZE;
		}
		if (input.isKeyPressed(Input.KEY_DOWN)) { 
			dy += World.TILE_SIZE;
		}
		if (input.isKeyPressed(Input.KEY_UP)) {
			dy -= World.TILE_SIZE;
		}
		
		// make sure the frog stays on screen
		if (getX() + dx - World.TILE_SIZE / 2 < 0 || getX() + dx + World.TILE_SIZE / 2 	> App.SCREEN_WIDTH) {
			dx = 0;
		}
		if (getY() + dy - World.TILE_SIZE / 2 < 0 || getY() + dy + World.TILE_SIZE / 2 > App.SCREEN_HEIGHT) {
			dy = 0;
		}
		
		/* keep track of previous position */
		prevX = getX();
		prevY = getY();
		
		if (this.speed != -1f) {
			move(this.speed*delta*(moveRight ? 1 : -1) + dx, dy);
			immune = true;
		}
		else {
			if (canMove) {
				move(dx, dy);
			} else {
				canMove = true;
			}
			immune = false;
		}
		//System.out.println(this.speed);
		
		// check if pushed off screen
		if (!onScreen()) {
			prevLives = lives;
			lives -= 1;
			restartPos();
		} else {
			prevLives = lives;
		}
		
		this.speed = NOT_ON_RIDEABLE;
		
		
	}
	
	/**
	 * Handles the player when it collides with a sprite.	
	 */
	@Override
	public void onCollision(Sprite other) {
		// when player collides with solid object
		if (other.hasTag(Sprite.SOLID)) {
			canMove = false;
			this.setX(prevX);
			this.setY(prevY);
			
		} 
		
		if (!other.hasTag(Sprite.SOLID)){
			canMove = true;
		}
		
		//collides with extra life
		if (other.hasTag(LIFE)) {
			lives += 1;
		}
		
		//collides with rideable
		if (other.hasTag(RIDEABLE)) {
			immune = true;
			if (other instanceof RideableObject) {
				rideable = ((RideableObject)other);
				this.speed = RideableObject.getSpeed((RideableObject)other);
				this.moveRight = rideable.getDir();
			}
			
			if (other instanceof Turtle) {
				if (!((Turtle)other).getOnSurface()) {
					rideable = ((RideableObject)other);
					lives -= 1;
					restartPos();
				}
				this.speed = ((Turtle)rideable).getSpeed();
				this.moveRight = rideable.getDir();
			}
		}
		
		// collides with hazard
		if (other.hasTag(Sprite.HAZARD) && !immune) {
			lives -= 1;
			restartPos();
		}
	}
	
	/**
	 * Restarts the player's position.
	 */
	public void restartPos() {
		if (lives < 0) {
			System.exit(0);
			// perhaps do an end game animation? 
			// go through sprite list and remove each one until none left
		}
		setX(App.SCREEN_WIDTH / 2);
		setY(App.SCREEN_HEIGHT - World.TILE_SIZE);
	}
	
	/**
	 * Getter for the flag when the player can/cannot move.
	 * @return Returns true/false if a player can move.
	 */
	public boolean getCanMove() {
		return canMove;
	}
	
	/**
	 * Setter for the canMove flag.
	 * @param canMove Either true/false if a player can move.
	 */
	public void setCanMove(boolean canMove) {
		this.canMove = canMove;
	}
	
	/**
	 * Getter for the amount of lives the player has.
	 * @return Returns the number of player lives.
	 */
	public int getLives() {
		return lives;
	}
	
	/** 
	 * Getter for the previous amount of lives the player had.
	 * @return Returns an int
	 */
	public int getPrevLives() {
		return prevLives;
	}
	
	/**
	 * Getter for the log or long log that the player is on.
	 * @return Returns the log or long log (RideableObject)
	 */
	public RideableObject getRideable() {
		return rideable;
	}
	
	/**
	 * Setter for the player's immunity to hazard
	 * @param immune Either true/false if immune (on a log/long log).
	 */
	public void setImmunity(boolean immune) {
		this.immune = immune;
	}
}
