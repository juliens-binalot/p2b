import org.newdawn.slick.Input;

/**
 * A class that can be collected by the player
 * for more lives.
 *
 */
public class ExtraLife extends Sprite{
	/** Address of the extra life image */
	public static final String ASSET_PATH = "assets/extralife.png";
	
	private float speed;
	
	private boolean moveRight;
	private boolean dirOnRideable = true;
	
	private int moveTime = 2000;
	
	private int timeElapsed = 0;
	
	private float prevX;
	
	private int delta;
	
	/** Initialises the extra life to be generated
	 * 
	 * @param path The address of the extra life image
	 * @param x The x position of the sprite
	 * @param y The y position of the sprite
	 * @param moveRight The direction of the sprite is moving
	 * @param type A sprite of type Life
	 */
	public ExtraLife(String path, float x, float y, boolean moveRight, String type) {
		super(path, x, y, new String[] {type});
		this.moveRight = moveRight;	
	}
	
	/**
	 * Updates the amount of time passed in the game since extra life sprite 
	 * appeared/disappeared.
	 */
	@Override
	public void update(Input input, int delta) {
		this.delta = delta;
	}
	
	/** Handles the extra life when it collides with 
	 * a log or long log.
	 */
	public void onCollision(Sprite other) {
		
		if (other.hasTag(Sprite.RIDEABLE) && !other.hasTag(HAZARD)) {
			move(speed * delta * (moveRight ? 1 : -1), 0);

			// has to move every 2 seconds
			timeElapsed += delta;
			if (timeElapsed >= moveTime) {
				// reset timeElapsed
				timeElapsed = 0;
				
				prevX = getX();
				if (dirOnRideable) {
					setX(getX()+World.TILE_SIZE);
				} else {
					setX(getX()-World.TILE_SIZE);
				}
				
			}
			
		}
		
	}
	
	/** 
	 * Setter for the speed of the extra life on a rideable object
	 * @param speed The speed of the rideable object it is on
	 */
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
	/**
	 * Setter for the direction it is moving about on the rideable object
	 * @param moveRight True/false if moving right
	 */
	public void setDirOnRideable(boolean moveRight) {
		this.dirOnRideable = moveRight;
	}
    
	/**
	 * Getter for the direction it is moving about on the rideable object.
	 * @return True/false if moving right on the rideable
	 */
	public boolean getDirOnRideable() {
		return dirOnRideable;
	}
	
	/** Getter for the previous x position of the sprite. 
	 * 
	 * @return Return the previous posiiton of the sprite (float)
	 */
	public float getPrevX() {
		return prevX;
	}

}
