import utilities.BoundingBox;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * The Sprite class creates all sprites  in the game except
 * for the player and its lives.
 *
 */
public class Sprite {
	// this is a defined constant to avoid typos
	public final static String HAZARD = "hazard";
	public final static String SOLID = "solid";
	public final static String RIDEABLE = "rideable";
	public final static String LIFE = "life";

	/** A flag for when the object is not on a rideable object */
	public static float NOT_ON_RIDEABLE = -1f;

	private BoundingBox bounds;
	private Image image;
	private float x;
	private float y;

	private String[] tags;
	
	/** 
	 * Initialises sprite
	 * @param imageSrc The address of the sprite image
	 * @param x The x position
	 * @param y The y position
	 */
	public Sprite(String imageSrc, float x, float y) {
		setupSprite(imageSrc, x, y);
	}

	public Sprite(String imageSrc, float x, float y, String[] tags) {
		setupSprite(imageSrc, x, y);
		this.tags = tags;
	}
	
	/**
	 * Sets up sprite in the game, creates bounding boxes
	 * @param imageSrc The address of image of the sprite
	 * @param x
	 * @param y
	 */

	private void setupSprite(String imageSrc, float x, float y) {
		try {
			image = new Image(imageSrc);
		} catch (SlickException e) {
			e.printStackTrace();
		}

		this.x = x;
		this.y = y;

		bounds = new BoundingBox(image, (int) x, (int) y);

		tags = new String[0];
	}

	/** 
	 * Creates the tile sprites.
	 * @param sprite A Tile object
	 * @param x The x position of the tile
	 * @param y The y position of the tile
	 * @return Returns a Tile object
	 */
	public static Sprite createSprite(String sprite, float x, float y) {
		switch (sprite) {
		case "water":
			// System.out.println(sprite);
			return Tile.createWaterTile(x, y);
		case "grass":
			return Tile.createGrassTile(x, y);
		case "tree":
			return Tile.createTreeTile(x, y);
		}

		return null;
	}

	/**
	 * Creates all sprites in the game except for Tile objects and Player
	 * @param sprite The Sprite object
	 * @param x The x position
	 * @param y The y position
	 * @param dir True/false if the object is moving right
	 * @return Returns a new object of type Sprite
	 */
	public static Sprite createSprite(String sprite, float x, float y, boolean dir) {
		switch (sprite) {
		case "bus":
			return Vehicle.createBus(x, y, dir, Sprite.HAZARD);
		case "bulldozer":
			return new Bulldozer(Bulldozer.ASSET_PATH, x, y, dir, Sprite.SOLID);
		case "log":
			return RideableObject.createLog(RideableObject.LOG_PATH, x, y, dir, Sprite.RIDEABLE);
		case "longLog":
			return RideableObject.createLongLog(RideableObject.LONGLOG_PATH, x, y, dir, Sprite.RIDEABLE);
		case "racecar":
			return Vehicle.createRaceCar(x, y, dir, Sprite.HAZARD);
		case "bike":
			return new Bike(Bike.ASSET_PATH, x, y, dir, Sprite.HAZARD);
		case "turtle":
			return new Turtle(Turtle.ASSET_PATH, x, y, dir, Sprite.RIDEABLE);
		case "extraLife":
			return new ExtraLife(ExtraLife.ASSET_PATH, x, y, dir, Sprite.LIFE);
		}
		return null;
	}

	/**
	 * Sets the x position of the sprite.
	 * 
	 * @param x the target x position
	 */
	public final void setX(float x) {
		this.x = x;
		bounds.setX((int) x);
	}

	/**
	 * Sets the y position of the sprite.
	 * 
	 * @param y the target y position
	 */
	public final void setY(float y) {
		this.y = y;
		bounds.setY((int) y);
	}

	/**
	 * Accesses the x position of the sprite.
	 * 
	 * @return the x position of the sprite
	 */
	public final float getX() {
		return x;
	}

	/**
	 * Accesses the y position of the sprite.
	 * 
	 * @return the y position of the sprite
	 */
	public final float getY() {
		return y;
	}
	
	/** 
	 * Handles the movement of the Sprite
	 * @param dx Change in x position
	 * @param dy Change in y position
	 */
	public final void move(float dx, float dy) {
		setX(x + dx);
		setY(y + dy);
	}
	
	/** 
	 * Checks if the sprite is on screen, not applicable to RideableObjects
	 * @param x The x position
	 * @param y The y position
	 * @return Returns true if the object is on screen
	 */
	public final boolean onScreen(float x, float y) {
		return !(x + World.TILE_SIZE / 2 > App.SCREEN_WIDTH || x - World.TILE_SIZE / 2 < 0
				|| y + World.TILE_SIZE / 2 > App.SCREEN_HEIGHT || y - World.TILE_SIZE / 2 < 0);
	}
	
	/** Checks if the sprite is on screen */
	public final boolean onScreen() {
		return onScreen(getX(), getY());
	}
	
	/** 
	 * Checks if sprites are colliding
	 * @param other A sprite object that is colliding with this sprite
	 * @return Returns true if colliding with another sprite
	 */
	public final boolean collides(Sprite other) {
		return bounds.intersects(other.bounds);
	}

	public void update(Input input, int delta) {
	}

	public void onCollision(Sprite other) {
	}

	public void render() {
		image.drawCentered(x, y);
	}
	
	/** Checks if a sprite has a tag
	 * 
	 * @param tag Whether sprite is a rideable, solid, life or hazard
	 * @return true/false if tag is/isn't found 
	 */
	public boolean hasTag(String tag) {
		for (String test : tags) {
			if (tag.equals(test)) {
				return true;
			}
		}
		return false;
	}
	
	/** 
	 * Accesses the tags of a sprite
	 * @return Returns the tags a sprite has
	 */
	public String[] getTags() {
		return tags;
	}
	
	/**
	 * Accesses the bounding box of a sprite
	 * @return Returns the bounding box of this sprite
	 */
	public BoundingBox getBounds() {
		return bounds;
	}
}

