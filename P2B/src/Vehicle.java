import org.newdawn.slick.Input;

/**
 * A class containing hazards to the player
 *
 */

public class Vehicle extends Sprite {
	/** The address of the bus image */
	public static final String BUS_PATH = "assets/bus.png";
	
	private static final float BUS_SPEED = 0.15f;
	
	/** The address of the racecar image */
	public static final String RACECAR_PATH = "assets/racecar.png";
	
	private static final float RACECAR_SPEED = 0.5f;
	
	private float speed;
	
	private static Vehicle bus;
	private static Vehicle racecar;
	
	private boolean moveRight;
	
	/** 
	 * Getter for the initial position of the sprite on screen.
	 * @return Returns the initial x position.
	 */
	public final float getInitialX() {
		return moveRight ? -World.TILE_SIZE / 2
						 : App.SCREEN_WIDTH + World.TILE_SIZE / 2;
	}
	
	/**
	 * Initialises the vehicle sprite
	 * @param path The address of the image 
	 * @param x The x position of the vehicle
	 * @param y The y position of the vehicle
	 * @param moveRight The direction of the vehicle
	 * @param type Whether the sprite is rideable/hazard/solid object
	 */
	public Vehicle(String path, float x, float y, boolean moveRight, String type) {
		super(path, x, y, new String[] { type });
	
		this.moveRight = moveRight;
	}
	
	/**
	 * Creates and initialises the bus sprite
	 * @param x 
	 * @param y
	 * @param dir
	 * @param type
	 * @return Returns the bus object
	 */
	public static Vehicle createBus(float x, float y, boolean dir, String type) {
		/*call vehicle constructor */
		bus = new Vehicle(BUS_PATH, x, y, dir, type);
		bus.speed = BUS_SPEED;
		return bus;
		
	}
	
	/**
	 *  Creates and initialises the racecar sprite
	 * @param x
	 * @param y
	 * @param dir
	 * @param type
	 * @return Returns the racecar object.
	 */
	public static Vehicle createRaceCar(float x, float y, boolean dir, String type) {
		racecar = new Vehicle(RACECAR_PATH, x, y, dir, type);
		racecar.speed = RACECAR_SPEED;
		return racecar; 
	}
	
	/**
	 * Updates the vehicles' positions on screen
	 */
	@Override
	public void update(Input input, int delta) {
		move(speed * delta * (moveRight ? 1 : -1), 0);
		
		// check if the vehicle has moved off the screen
		if (getX() > App.SCREEN_WIDTH + World.TILE_SIZE / 2 || getX() < -World.TILE_SIZE / 2
		 || getY() > App.SCREEN_HEIGHT + World.TILE_SIZE / 2 || getY() < -World.TILE_SIZE / 2) {
			setX(getInitialX());
		}
	}
}
